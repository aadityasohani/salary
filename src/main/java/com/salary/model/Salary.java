package com.salary.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Salary {
    @Id
    private int age;
    private long salary;

    public Salary(int age, long salary) {
        this.age = age;
        this.salary = salary;
    }

    public Salary() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }
}
