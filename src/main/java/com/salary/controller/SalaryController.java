package com.salary.controller;


import com.salary.model.Salary;
import com.salary.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SalaryController {
    @Autowired
    SalaryService salaryService;


    @GetMapping("/getSalary/{age}")
    public long getSalary(@PathVariable int age){
//        System.out.println(age);
        Salary s = salaryService.getSalaryByAge(age);
//        System.out.println(s.getSalary());
        long x = s.getSalary();
        return x;
    }


    @PostMapping("/addSalary")
    public String addSalary(@RequestBody Salary s){
        return salaryService.setSalary(s);
    }

    @PostMapping("/addSalaries")
    public List<String> addSalary(@RequestBody List<Salary> salaryList){
        return salaryService.setSalaries(salaryList);
    }
}
