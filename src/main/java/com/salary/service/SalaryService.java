package com.salary.service;

import com.salary.model.Salary;
import com.salary.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SalaryService {
    @Autowired
    SalaryRepository salaryRepository;

    public Salary getSalaryByAge(int age){
        return salaryRepository.findSalaryByAge(age);
    }

    public String setSalary(Salary s){
        if(salaryRepository.existsById(s.getAge())){
            return "Age Already exists";
        }
        salaryRepository.save(s);
        return "Added Successfully";
    }

    public List<String> setSalaries(List<Salary> salaries){
        List<String> toReturn = new ArrayList<>();

        for(Salary salary : salaries){
            toReturn.add(this.setSalary(salary));
        }

        return toReturn;
    }
}
